#include <termios.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <stdbool.h>

typedef struct {
    uint8_t rows, cols;
    uint8_t cur_x, cur_y;
    bool* enabled;
} Game;

void init_game(Game* game, uint8_t rows, uint8_t cols) {
    int size = rows * cols;
    game->rows = rows;
    game->cols = cols;
    game->cur_x = 0;
    game->cur_y = 0;
    memset(game->enabled, false, sizeof(uint8_t) * size);
}

uint16_t cords_to_index(Game* game, uint8_t x, uint8_t y) {
    return (y * game->rows) + x;
}

void print_square(Game* game) {
    for (int y = 0; y < game->rows; ++y) {
        for (int x = 0; x < game->cols; ++x) {
            bool current = game->cur_x == x && game->cur_y == y;
            bool enabled = game->enabled[cords_to_index(game, x, y)];
            printf("%c%c%c", 
                current ? '[' : ' ',
                enabled ? '#' : '.',
                current ? ']' : ' '
            );
        }
        printf("\n");
    }
}

void reset_cursor(Game* game) { 
    printf("%c[%d%c", 27, game->rows,     'A');
    printf("%c[%d%c", 27, game->cols * 3, 'D');
}

void handle_input(Game* game, char c) {
    int dx = 0;
    int dy = 0;
    switch (c) {
        case 'w': {
            dy = -1;
        } break;
        case 's': {
            dy = 1;
        } break;
        case 'a': {
            dx = -1;
        } break;
        case 'd': {
            dx = 1;
        } break;
        case ' ': {
            uint16_t i = cords_to_index(game, game->cur_x, game->cur_y);
            bool enabled = game->enabled[i];
            game->enabled[i] = !enabled;
        } break;
    }
    if (!((int)game->cur_x + dx < 0 || (int)game->cur_x + dx>= game->cols))
        game->cur_x += dx;
    if (!((int)game->cur_y + dy < 0 || (int)game->cur_y + dy >= game->rows))
        game->cur_y += dy;
}

int main() {
    struct termios* TAttr = malloc(sizeof(struct termios));
    struct termios* SavedTAttr = malloc(sizeof(struct termios));
    // Get currecnt termios and make a backup
    tcgetattr(STDIN_FILENO, TAttr); 
    tcgetattr(STDIN_FILENO, SavedTAttr); 

    // Magic to hide input
    TAttr->c_lflag &= !(ICANON | ECHO);
    TAttr->c_cc[VMIN] = 1;
    TAttr->c_cc[VTIME] = 0;
    tcsetattr(STDIN_FILENO, TCSAFLUSH, TAttr);
   
    const uint8_t rows = 20, cols = 20;
    Game game = {};
    game.enabled = malloc(sizeof(bool) * (rows * cols));
    init_game(&game, rows, cols);

    print_square(&game);

    char c;
    while ((c = getchar()) != 'q') {
        handle_input(&game, c);
        reset_cursor(&game);
        print_square(&game);
    }

    tcsetattr(STDIN_FILENO, TCSANOW, SavedTAttr);
}
